package project.mapper;

import org.springframework.stereotype.Component;
import project.dto.UserDto;
import project.model.User;

@Component
public class UserMapper {

    public UserDto mapToDto(User user) {
        return UserDto.builder()
                .name(user.getName())
                .build();
    }

    public User mapToUser(UserDto dto) {
        User user = new User();
        user.setName(dto.getName());
        return user;
    }

}
