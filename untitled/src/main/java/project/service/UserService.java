package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.UserDto;
import project.mapper.UserMapper;
import project.model.User;
import project.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;
    @Autowired
    private UserMapper mapper;

    public UserDto addUser(UserDto dto) {
        User saved = repository.save(mapper.mapToUser(dto));
        return mapper.mapToDto(saved);
    }

}
