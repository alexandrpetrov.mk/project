package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import project.dto.UserDto;
import project.service.UserService;

@RestController
public class UserController {
    @Autowired
    private UserService service;

    @PostMapping("/add")
    public UserDto addUser(@RequestBody UserDto userDto) {
        return service.addUser(userDto);
    }
}
